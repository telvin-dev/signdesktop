#!/bin/bash

mkdir /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset
sips -z 16 16 /Users/apple/Documents/XcodeProjects/signdesktop/logo_1024.jpg --out /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset/icon_16x16@1x.jpg
sips -z 32 32 /Users/apple/Documents/XcodeProjects/signdesktop/logo_1024.jpg --out /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset/icon_16x16@2x.jpg
sips -z 32 32  /Users/apple/Documents/XcodeProjects/signdesktop/logo_1024.jpg --out /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset/icon_32x32@1x.jpg
sips -z 64 64 /Users/apple/Documents/XcodeProjects/signdesktop/logo_1024.jpg --out /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset/icon_32x32@2x.jpg
sips -z 128 128 /Users/apple/Documents/XcodeProjects/signdesktop/logo_1024.jpg --out /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset/icon_128x128@1x.jpg
sips -z 256 256 /Users/apple/Documents/XcodeProjects/signdesktop/logo_1024.jpg --out /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset/icon_128x128@2x.jpg
sips -z 256 256 /Users/apple/Documents/XcodeProjects/signdesktop/logo_1024.jpg --out /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset/icon_256x256@1x.jpg
sips -z 512 512 /Users/apple/Documents/XcodeProjects/signdesktop/logo_1024.jpg --out /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset/icon_256x256@2x.jpg
sips -z 512 512 /Users/apple/Documents/XcodeProjects/signdesktop/logo_1024.jpg --out /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset/icon_512x512@1x.jpg
cp /Users/apple/Documents/XcodeProjects/signdesktop/logo_1024.jpg /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset/icon_512x512@2x.jpg
iconutil -c icns /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset
# rm -R /Users/apple/Documents/XcodeProjects/signdesktop/ssDesktop.iconset