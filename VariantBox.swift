//
//  VariantBox.swift
//  signdesktop
//
//  Created by apple on 3/31/16.
//  Copyright © 2016 signsmart. All rights reserved.
//

import Cocoa

class VarianBox: NSView{

    @IBOutlet weak var background: NSImageView!
    
    override func drawRect(dirtyRect: NSRect) {
        background.image = (NSImage(named: "shadow_box") as! AnyObject) as? NSImage
        super.drawRect(dirtyRect)
    }
    
}
