//
//  StrokeLabel.swift
//  signdesktop
//
//  Created by apple on 3/30/16.
//  Copyright © 2016 signsmart. All rights reserved.
//

import Cocoa

class StrokeLabel: NSTextField{

    override func drawRect(rect: NSRect) {
        
        //let font =  NSFont(name: "Arial-Black", size: 10)

//        let textFontAttributes: [String:AnyObject] = [
//            //NSFontAttributeName: font!,
//            NSForegroundColorAttributeName: NSColor.whiteColor(),
//            NSStrokeWidthAttributeName: Int(-2.0),
//            NSStrokeColorAttributeName: NSColor.blackColor()
//        ];
        
        // Paint the background
        //NSColor.grayColor().set()
        NSBezierPath.fillRect(self.bounds)
        // Draw the string
        
        var path: NSBezierPath
        path = NSBezierPath(rect: rect)
        path.lineWidth = 2
        NSColor(calibratedWhite: 1.0, alpha: 0.394).set()
        path.fill()
        NSColor.redColor().set()
        path.stroke()
        super.attributedStringValue.drawInRect(rect)
    }
    
    override func d

}
