import Foundation
import AppKit
import CocoaLumberjack
import SVGKit
import ProgressKit

class _ToolbarCellCVI: NSCollectionViewItem{
    
    
    @IBOutlet weak var icon: NSImageView!
    @IBOutlet weak var svgIcon: SVGKLayeredImageView!
    
    
    var toolbar:Toolbar? {
        return super.representedObject as? Toolbar
    }
    
    override var representedObject: AnyObject? {
        get {
            return super.representedObject
        }
        set(newRepresentedObject) {
            if(newRepresentedObject != nil){
                super.representedObject = newRepresentedObject
                configureCell(self.toolbar!)

            }
        }
    }
    
    override var selected: Bool {
        didSet {
            (self.view as! _ToolbarCellView).selected = selected
        }
    }
    override var highlightState: NSCollectionViewItemHighlightState {
        didSet {
            (self.view as! _ToolbarCellView).highlightState = highlightState
        }
    }
    
    // MARK: NSResponder

    override func mouseDown(theEvent: NSEvent) {
        if theEvent.clickCount == 2 {
        } else {
            super.mouseDown(theEvent) // proceed if this is not double click
        }
        
    }
    
    func configureCell(toolbar: Toolbar){
        if let path = toolbar.icon{
            
            if let url = NSURL(string: path){
                if let pathExtension = url.pathExtension as String?{

                    if pathExtension.lowercaseString == "svg"{
                        
                        if let svgImg = SVGKImage(contentsOfURL: url){
                            self.svgIcon.image = svgImg
                        }

                        
                        self.svgIcon.image = SVGKImage(contentsOfURL: url)
                        self.svgIcon.hidden = false;
                        self.layoutIcon(self.svgIcon)
                        
                        
                    }
                    else{
                        self.svgIcon.hidden = true;
                        self.icon.image = NSImage(named: url.URLByDeletingPathExtension!.lastPathComponent!)
                        if((self.icon.image == nil)){
                            self.icon.image = NSImage.init(contentsOfURL: url)
                        }
                        self.layoutIcon(self.icon)

                    }
                    
                }
            }
            
        }
    }
    
    func layoutIcon(icon: NSView){
        icon.layer!.masksToBounds = true

    }

    func activeToolbar(selected: Bool){
        self.view.alphaValue = (selected) ? 2: 0.8
    }
    
    
}