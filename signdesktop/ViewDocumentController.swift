//
//  ViewDocumentController.swift
//  signdesktop

import Cocoa
import Alamofire
import SwiftyJSON
import FileKit
import AVKit
import AVFoundation



import Cocoa
import Alamofire
import SwiftyJSON
import FileKit
import AVKit
import AVFoundation



struct ViewDocumentController{


    var ovid: String!

    init(ovid: String){
        self.ovid = ovid
        self.getDocument()
    }

    func getDocument(){
        let getVideoInfoUrl = CommonHelper.makeApiUrl("/osx/getdocument");
        let(_,_,_,acc_id) = MembershipHelper.getCurrentUserAccessing()
        if let _ovid = self.ovid {
            let params = [
                "acc_id": acc_id!,
                "ovid": _ovid
            ]
            Alamofire.request(.POST, getVideoInfoUrl, parameters: params)
                .responseJSON { response in
                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            if let json: JSON? = JSON(value){
                                if json!["OK"].number!.integerValue == 1{
                                    let url = NSURL(string: json!["content"].rawString()!)
                                    NSWorkspace.sharedWorkspace().openURL(url!)
                                }
                            }
                        }
                    case .Failure(let error):
                        print(error)
                    }
            }
        }
    }
    
}

