import Cocoa
import FileKit
import Alamofire
import SVGKit
import PromiseKit

extension ContentController {
    func loadToolbars_Collection() -> Promise<Void> {

        let params = [:]
        
        return Promise{ fulfill, reject in
            
            self.listToolbarCollectionView.alphaValue = 0.5;

            ToolbarHelper.getListToolbars_Promise(params).then{ listToolbars -> Void in
                
                self.listToolbarCollection = listToolbars;
                self.listToolbarCollectionView.reloadData()
                self.listToolbarCollectionView.performBatchUpdates({}, completionHandler: {
                    (finished: Bool) in
                    self.updateProgressBar("loadToolbars_Collection");
                    fulfill(())
                })
            }
            self.listToolbarCollectionView.alphaValue = 1;
            fulfill(())
            
        }
        
    }    

}
