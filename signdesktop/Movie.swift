
import Foundation
import SwiftyJSON

class Movie{
    var URL_BASE = "http://image.tmdb.org/t/p/w500"
    var movieId: String!
    var title: String!
    var overview: String!
    var posterPath: String!
    
    init(movieDict: Dictionary<String, AnyObject>){
        if let title = movieDict["title"] as? String{
            self.title = title
        }
        
        if let overview = movieDict["overview"] as? String {
            self.overview = overview
        }
        
        if let path = movieDict["poster_path"] as? String{
            self.posterPath = "\(URL_BASE)\(path)"
        }
        
        if let movie_id = movieDict["id"] as? NSInteger{
            self.movieId = String(movie_id)
        }
    }
    
    init(movieJSON: JSON){
        if let title = movieJSON["title"].rawString() {
            self.title = title
        }
        
        if let overview = movieJSON["overview"].rawString() {
            self.overview = overview
        }
        
        if let path = movieJSON["poster_path"].rawString(){
            self.posterPath = "\(URL_BASE)\(path)"
        }
        
        if let movie_id = movieJSON["id"].number{
            self.movieId = String(movie_id)
        }
    }
    
}
