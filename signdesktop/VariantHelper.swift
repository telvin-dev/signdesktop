
import Foundation
import Alamofire
import SwiftyJSON
import PromiseKit


struct VariantHelper{

    static func getListVariants_Promise(params: NSDictionary)->Promise<NSDictionary>{

        return Promise{ fulfil, reject in

            var list = [ObjectVariant]()
            var data = Dictionary<String, AnyObject>()
            let fetchDataUrl = CommonHelper.makeApiUrl("osx/getlistvariants");

            Alamofire.request(.POST, fetchDataUrl, parameters: params as? [String : AnyObject])
                .responseJSON {
                    response in
                    switch response.result {
                    case .Success:
                        // reset
                        if let value = response.result.value {
                            if let   json: JSON? = JSON(value){
                                if(json?.isEmpty == false){
                                    if json!["OK"].number!.integerValue == 1{
                                        if let _list: JSON? = json!["list"]{
                                            for (_,entry):(String, JSON) in _list! {
                                                let ov = ObjectVariant(ov: entry)
                                                list.append(ov)
                                            }
                                        }
                                        data["list"] = list
                                        data["pageCount"] = json!["pageCount"].int
                                        fulfil(data)
                                    }
                                }
                            }
                        }
                    case .Failure(let error):
                        reject(error)
                    }
            }

        }
    }

    static func autoShowScrollBtn(pageIndex: Int, pageCount: Int, leftScroll: NSButton, rightScroll : NSButton) {
        if(pageIndex == 1 && pageCount > 1){
            rightScroll.hidden = false;
            leftScroll.hidden = true;
        }
        else if(1 < pageIndex && pageIndex < pageCount){
            rightScroll.hidden = false;
            leftScroll.hidden = false;
        }
        else if(pageIndex == pageCount && pageCount > 1){
            rightScroll.hidden = true;
            leftScroll.hidden = false;
        }else{
            rightScroll.hidden = true;
            leftScroll.hidden = true;
            
        }
    }
    
}


