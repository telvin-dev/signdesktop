//
//  uploadVideoController.swift
//  signdesktop

import Cocoa
import Alamofire
import SwiftyJSON
import FileKit

class UploadVideoController: NSViewController  {
    
    @IBOutlet weak var indicatorView: NSProgressIndicator!    
    @IBOutlet weak var uploadBtn: NSButton!
    @IBOutlet weak var videoName: NSTextField!
    
    
    var videoPath: NSURL?

    var oid: String!

    var parentVc: ContentController!

    override func viewDidLoad() {
        self.uploadBtn.enabled = false
    }


    
    @IBAction func selectVideo(sender: AnyObject) {
        let openPanel = NSOpenPanel()
        openPanel.allowsMultipleSelection = false
        openPanel.canChooseDirectories = false
        openPanel.canCreateDirectories = false
        openPanel.canChooseFiles = true

        openPanel.allowedFileTypes = self.getValidExt(self.oid) as? [String]

        openPanel.beginWithCompletionHandler { (result) -> Void in
            if result == NSFileHandlingPanelOKButton {
                
                self.videoPath = openPanel.URL
                self.videoName.stringValue = (self.videoPath! as NSURL).lastPathComponent!
                if(self.videoPath != nil){
                    self.uploadBtn.enabled = true;
                }
                
            }
        }
    }
    
    @IBAction func uploadVideo(sender: AnyObject) {
        
        self.indicatorView.startAnimation(self.uploadBtn)
        let path = self.videoPath?.path
        let parameters = ["name": self.videoName.stringValue]
        let urlRequest = FileHelper.urlRequestWithComponents(CommonHelper.makeApiUrl("osx/uploadvideo"), parameters: parameters, path: Path(path!))
        
        Alamofire.upload(urlRequest.0, data: urlRequest.1)
            .progress { bytesWritten, totalBytesWritten, totalBytesExpectedToWrite in
                
                dispatch_async(dispatch_get_main_queue()) {
                    let percent = Double(Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)*100);
                    self.indicatorView.doubleValue = percent
                    self.indicatorView.needsDisplay = true
                }
            }
            .responseJSON { response in
                self.indicatorView.stopAnimation(self.uploadBtn)
                self.view.window?.close()

                self.parentVc.loadVariants_Item().then{ Void in
                    self.parentVc.toggleObjectVariant(VariantKind.ITEM)
                }


            }
        
    }

    func getValidExt(oid: String) -> NSArray{

        var validExt = []
        switch oid {
        case "6": //video
            validExt = ["mp4"]
        case "2": //image
            validExt = ["jpg", "jpeg", "png"]
        case "1": //document
            validExt = ["doc","docx","txt","pdf"]
        default: break

        }
        return validExt
    }
    
}

