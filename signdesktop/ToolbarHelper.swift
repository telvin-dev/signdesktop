
import Foundation
import Alamofire
import SwiftyJSON
import PromiseKit


struct ToolbarHelper{
    
    static func getListToolbars_Promise(params: NSDictionary)->Promise<[Toolbar]>{
        
        return Promise{ fulfil, reject in
            
            var list = [Toolbar]()
            let fetchDataUrl = CommonHelper.makeApiUrl("osx/gettoolbars");
            
            
            Alamofire.request(.POST, fetchDataUrl, parameters: params as? [String : AnyObject])
                .responseJSON {
                    response in
                    
                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            if let   json: JSON? = JSON(value){
                                
                                if json!["OK"].number!.integerValue == 1{
                                    
                                    if let _list: JSON? = json!["list"]{
                                        for (_,entry):(String, JSON) in _list! {
                                            let tb = Toolbar(ov: entry)
                                            list.append(tb)
                                        }
                                    }
                                    
                                    fulfil(list)
                                }else{
                                    
                                }
                                
                            }
                            
                        }
                    case .Failure(let error):
                        reject(error)
                    }
            }
            
        }
        
    }
    
}


