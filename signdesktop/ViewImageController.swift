//
//  ViewImageController.swift
//  signdesktop


import Cocoa
import Alamofire
import SwiftyJSON
import FileKit
import AVKit
import AVFoundation



class ViewImageController: NSViewController, MBProgressHUDDelegate{


    var ovid: String!
    var myImageView: NSImageView!
    var myScrollView: NSScrollView!

    var loader: MBProgressHUD!;

    var appWindowController: NSWindowController!

    override func viewDidLoad() {

        self.loader = ViewHelper.addHUD(self, viewDelegate: self)
        self.loader.show(true)
        self.getImage()
    }

    func getImage(){
        let getVideoInfoUrl = CommonHelper.makeApiUrl("/osx/getimage");
        let(_,_,_,acc_id) = MembershipHelper.getCurrentUserAccessing()
        if let _ovid = self.ovid {
            let params = [
                "acc_id": acc_id!,
                "ovid": _ovid
            ]
            Alamofire.request(.POST, getVideoInfoUrl, parameters: params)
                .responseJSON { response in
                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            if let json: JSON? = JSON(value){
                                if json!["OK"].number!.integerValue == 1{
                                    let urlImage = NSURL(string: json!["content"].rawString()!)
                                    let myImage: NSImage = NSImage(contentsOfURL: urlImage!)!

                                    let screenSize = NSScreen.mainScreen()?.frame
                                    let x = ((screenSize?.width)! - myImage.size.width) / 2.0;
                                    let y = ((screenSize?.height)! - myImage.size.height) / 2.0;
                                    let imageRect: NSRect = NSMakeRect(x, y, myImage.size.width, myImage.size.height)
                                    self.myImageView = NSImageView(frame: imageRect)
                                    self.myImageView.bounds = imageRect
                                    self.myImageView.image = myImage
                                    self.view.addSubview(self.myImageView)
                                    self.loader.show(false)
                                    ViewHelper.removeHUD(self.loader)

                                }
                            }
                        }
                    case .Failure(let error):
                        print(error)
                    }
            }
        }
    }
    
}





