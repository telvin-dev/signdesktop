import Foundation
import AppKit
import CocoaLumberjack
import SVGKit
import ProgressKit

class _ToolbarActionCVI: NSCollectionViewItem{
    
    
    @IBOutlet weak var actionName: NSTextField!
    @IBOutlet weak var button: NSImageView!
   // @IBOutlet weak var button: NSButton!
    var execFunction: String?
    
    var action:ToolbarAction? {
        return super.representedObject as? ToolbarAction
    }
    
    override var representedObject: AnyObject? {
        get {
            return super.representedObject
        }
        set(newRepresentedObject) {
            if(newRepresentedObject != nil){
                super.representedObject = newRepresentedObject
                configureCell(self.action!)
                
            }
        }
    }
    
    override var selected: Bool {
        didSet {
            (self.view as! _ToolbarActionView).selected = selected
        }
    }
    override var highlightState: NSCollectionViewItemHighlightState {
        didSet {
            (self.view as! _ToolbarActionView).highlightState = highlightState
        }
    }
    
    // MARK: NSResponder
    
    override func mouseDown(theEvent: NSEvent) {
        if theEvent.clickCount == 2 {
        } else {
            super.mouseDown(theEvent) // proceed if this is not double click
        }
        
    }
    
    func configureCell(action: ToolbarAction){
        
        if let actionName = action.name{
            self.actionName.stringValue = actionName
            self.execFunction = action.exec_function
        }
        
    }
    
    func layoutIcon(icon: NSView){
        
        icon.layer!.masksToBounds = true
    }
    
}