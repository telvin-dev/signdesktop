import Foundation
import AppKit
import CocoaLumberjack
import SVGKit
import ProgressKit
import OTWebImage


class _ObjectVariantCVI: NSCollectionViewItem{


    @IBOutlet weak var icon: NSImageView!
    @IBOutlet weak var svgIcon: SVGKLayeredImageView!
    @IBOutlet weak var extFile: NSTextField!
    @IBOutlet weak var name: NSTextField!

    var ov:ObjectVariant? {
        return super.representedObject as? ObjectVariant
    }

    override var representedObject: AnyObject? {
        get {
            return super.representedObject
        }
        set(newRepresentedObject) {
            if(newRepresentedObject != nil){
                super.representedObject = newRepresentedObject
                configureCell(self.ov!)

            }
        }
    }

    override var selected: Bool {
        didSet {
            (self.view as! _ObjectVariantView).selected = selected
        }
    }
    override var highlightState: NSCollectionViewItemHighlightState {
        didSet {
            (self.view as! _ObjectVariantView).highlightState = highlightState
        }
    }

    // MARK: NSResponder

    override func mouseDown(theEvent: NSEvent) {
        if theEvent.clickCount == 2 {
        } else {
            super.mouseDown(theEvent) // proceed if this is not double click
        }

    }

    func configureCell(ov: ObjectVariant){
        self.name.stringValue = ov.name
        if let path = ov.icon{
            if let url = NSURL(string: path){
                if let pathExtension = url.pathExtension as String?{

                    //load icon image
                    self.svgIcon.hidden = true;
                    self.icon.image = NSImage(named: url.URLByDeletingPathExtension!.lastPathComponent!)
                    self.icon.imageURL = url
                    if((self.icon.image == nil)){
                        if pathExtension.lowercaseString == "svg"{
                            do{
                                self.svgIcon.image = SVGKImage(contentsOfURL: url)
                                self.svgIcon.hidden = false;
                            }catch let error as NSException{
                            }
                        }else{
                            self.icon.image = NSImage.init(contentsOfURL: url)
                        }
                    }
                    self.layoutIcon(self.icon)
                    self.setLableExtVideo(self.extFile, ov: ov)  //spin lable extension for video item

                }
            }
        }
    }

    func layoutIcon(icon: NSView){
        icon.layer!.masksToBounds = true
        icon.layer!.cornerRadius = 20;
    }

    func setLableExtVideo(lable: NSTextField, ov: ObjectVariant){
        if(ov.extVideo != ""){
            self.extFile.hidden = false
            self.extFile.drawsBackground = true
            self.extFile.stringValue = ov.extVideo.lowercaseString
            self.extFile.backgroundColor = (ov.extVideo == "mp4") ? NSColor(red:0.18, green:0.71, blue:0.035, alpha:1) : NSColor(red:0.859, green:0.059, blue:0.059, alpha:1)
        }else{
            self.extFile.hidden = true
            self.extFile.drawsBackground = false;
        }

    }

}

