import Foundation
import AppKit
import CocoaLumberjack
import SVGKit
import ProgressKit


class _ObjectVariantView: NSView{
    
    var selected: Bool = false {
        didSet {
            if selected != oldValue {
                needsDisplay = true
            }
        }
    }
    var highlightState: NSCollectionViewItemHighlightState = .None {
        didSet {
            if highlightState != oldValue {
                needsDisplay = true
            }
        }
    }
    
    // MARK: NSView
    
    override var wantsUpdateLayer: Bool {
        return true
    }
    
    /* this function is raised after the layer is updated on ToolbarCell Collection View Item */
    override func updateLayer() {
        
        if selected {
            layer!.borderColor = NSColor.yellowColor().CGColor
            layer!.borderWidth = 3.0

        } else {
            
            layer!.borderColor = NSColor.clearColor().CGColor
            layer!.borderWidth = 0.0
        }
        
    }
    
    // MARK: init
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        wantsLayer = true
        layer?.masksToBounds = true
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        wantsLayer = true
        layer?.masksToBounds = true
    }
    
}