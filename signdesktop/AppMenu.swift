//
//  Menu.swift
//  signdesktop
//
import Cocoa


class AppMenu: NSObject, NSApplicationDelegate{
    
    

    @IBAction func logout(sender: NSMenuItem) {
        
        let mainSb = NSStoryboard(name: "Main", bundle: nil)
        let loginVC = mainSb.instantiateControllerWithIdentifier("LoginController")
            
        NSApplication.sharedApplication().keyWindow?.contentViewController = loginVC as? LoginController
        
    }
    
}