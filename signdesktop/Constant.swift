//
//  Constant.swift
//  signdesktop
//
//  Created by apple on 3/4/16.
//  Copyright © 2016 signsmart. All rights reserved.
//

import Foundation

struct Constant {
    static var SS_API_HOST = "https://signsmart.com/dev";
    
    static var UNLIMIT = 1000000
}

