//
//  _ContentController_Paging.swift
//  signdesktop
//



import Cocoa
import AppKit
import Alamofire
import SVGKit
import PromiseKit

extension ContentController{

    //Item Variant
    @IBAction func leftScroll_Item(sender: AnyObject) {
        if(self.ivPageIndex > 1){
            self.ivPageIndex -= 1;
            self.loadVariants_Item();
        }

    }
    @IBAction func rightScroll_Item(sender: AnyObject) {
        if(self.ivPageIndex < self.ivPageCount){
            self.ivPageIndex += 1;
            self.loadVariants_Item()
        }
    }

    //Group Variant
    @IBAction func leftScroll_Group(sender: AnyObject) {
        if(self.gvPageIndex > 1){
            self.gvPageIndex -= 1;
            self.loadVariants_Group();
        }
    }
    @IBAction func rightScroll_Group(sender: AnyObject) {
        if(self.gvPageIndex < self.gvPageCount){
            self.gvPageIndex += 1;
            self.loadVariants_Group()
        }
    }

    //Collection Variant
    @IBAction func leftScroll_Collection(sender: AnyObject) {
        if(self.cvPageIndex > 1){
            self.cvPageIndex -= 1;
            self.loadVariants_Collection();
        }
    }
    @IBAction func rightScroll_Collection(sender: AnyObject) {
        if(self.cvPageIndex < self.cvPageCount){
            self.cvPageIndex += 1;
            self.loadVariants_Collection()
        }
    }
}










