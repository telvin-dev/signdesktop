
import Foundation
import SwiftyJSON

class Toolbar {
    
    var oid: String!
    var mtype: String!
    var toolbar_extension: String!
    var icon: String!
    var name: String!
    
    
    var support_cids: [String!] = []
    var support_gids: [String!] = []
    var support_iids: [String!] = []

    
    init(ov: JSON){
        if let oid = ov["oid"].rawString(){
            self.oid = oid
        }
        
        if let mtype = ov["mtype"].rawString(){
            self.mtype = mtype
        }
        
        if let name = ov["name"].rawString() {
            self.name = name
        }
        
        
        if let icon = ov["icon"].rawString() {
            self.icon = icon
        }
        
        if let toolbar_extension = ov["toolbar_extension"].rawString(){
            self.toolbar_extension = toolbar_extension
        }
        
        if let support_cids = ov["support_cid"].arrayObject as? [NSNumber]{
            
            
            self.support_cids = support_cids.map(
                {
                    (number: NSNumber) -> String! in
                    return number.stringValue
            })
        }
        
        if let support_gids = ov["support_gid"].arrayObject as? [NSNumber]{
            self.support_gids = support_gids.map(
                {
                    (number: NSNumber) -> String! in
                    return number.stringValue
            })
        }
        
        if let support_iids = ov["support_iid"].arrayObject as? [NSNumber]{
            
            self.support_iids = support_iids.map(
                {
                    (number: NSNumber) -> String! in
                    return number.stringValue
            })
        }
    }
    
}
