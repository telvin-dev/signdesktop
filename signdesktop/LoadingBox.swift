
import Cocoa
import Alamofire
import SwiftyJSON


class LoadingBox: NSView {
    
    
    @IBOutlet var view: NSView!
    @IBOutlet weak var loadingBox: NSBox!
    @IBOutlet weak var progressBar: NSProgressIndicator!
    @IBOutlet weak var retryBtn: NSButton!
    @IBOutlet weak var message: NSTextField!
    
    var contentHolder: ContentController!
    
    @IBAction func retryOnclick(sender: NSButton) {

    }
    
    func start(){
        self.progressBar.doubleValue = 0.0
        self.message.stringValue = "Loading..."
        self.retryBtn.hidden = true;
        self.progressBar.hidden = false
        self.view.hidden = false;
    }
    
    func hasProblem(){
        self.message.stringValue = "Due to network issue, the data cannot be transfered thoroughly."
        self.retryBtn.hidden = false;
        self.progressBar.hidden = true
        self.view.hidden = false;
        
    }

    
    func finish(){
        self.progressBar.doubleValue = 100.0
        self.message.stringValue = "Done"
        self.retryBtn.hidden = true;
        self.progressBar.hidden = false
        self.view.hidden = true;

    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        NSBundle.mainBundle().loadNibNamed("LoadingBox", owner: self, topLevelObjects: nil)
        self.wantsLayer = true;
        self.addSubview(self.view)

    }
    
    override func drawRect(dirtyRect: NSRect) {
        self.loadingBox.layer?.cornerRadius = 10.0
        self.view.layer?.cornerRadius = 10.0
        self.progressBar.doubleValue = 20.0
        super.drawRect(dirtyRect)
    }
}

