
import Foundation


struct CommonHelper{

    static func makeApiUrl(controllerAction: String) -> String {
        return "\(Constant.SS_API_HOST)/\(controllerAction)"
    }

    static func invoke( selector:Selector, on target:AnyObject, afterDelay delay:NSTimeInterval ) {

        NSTimer.scheduledTimerWithTimeInterval( delay, target: target, selector: selector, userInfo: nil, repeats: false )
    }

}

