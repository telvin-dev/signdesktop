//
// Created by apple on 2/11/16.
// Copyright (c) 2016 signsmart. All rights reserved.
//

import Foundation
import SwiftyJSON

class ToolbarAction {
    var name: String!
    var exec_function: String!
    
    
    init(actionJSON: JSON){
        
        if let name = actionJSON["name"].rawString() {
            self.name = name
        }
        
        if let exec_function = actionJSON["exec_function"].rawString() {
            self.exec_function = exec_function
        }

        
    }

}
