//
//  Trailer.swift
//  ss-tvapp
//
//  Created by apple on 3/1/16.
//  Copyright © 2016 signsmart. All rights reserved.
//

import Foundation
import SwiftyJSON

class MovieTrailer : ObjectVariant{
    var URL_BASE = "http://image.tmdb.org/t/p/w500"
    
    init(movieJSON: JSON){
        super.init()
        
        if let title = movieJSON["title"].rawString() {
            self.name = title
        }
        
        if let overview = movieJSON["overview"].rawString() {
            self.toolbar_name = overview
        }
        
        if let path = movieJSON["poster_path"].rawString(){
            self.icon = "\(URL_BASE)\(path)"
        }
        
        if let movie_id = movieJSON["id"].number{
            self.ovid = String(movie_id)
        }
        
        self.toolbar_extension = SignSmartExtension.YoutubeVideo
        //        if let movie_id = movieJSON["id"].number{
        //            self.movieId = movie_id.integerValue
        //        }
    }

}
