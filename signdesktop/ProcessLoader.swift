
import Foundation
import AppKit

class ProcessLoader{
    var list: [String:String]!;
    let _hud:MBProgressHUD!;
    
    init(hud: MBProgressHUD){
        list = [String:String]()
        self._hud = hud;
    }
    
    func addProcess(name: String) -> String{
        
        if(list.count == 0){
            if(_hud != nil){
                
                _hud.show(true)
            }
            
        }
        
        let processId = NSUUID().UUIDString
        list[processId] = name;
        return processId;
        
    }
    
    func removeProcess(pid: String){
        list.removeValueForKey(pid)
        
        if(list.count == 0){
            if(_hud != nil){
                
                _hud.hide(true)
            }
        }
        
        
        
    }
    
}
