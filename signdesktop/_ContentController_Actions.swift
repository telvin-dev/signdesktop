import Cocoa
import AppKit
import Alamofire
import SVGKit
import PromiseKit

extension ContentController{


    func loadToolbarActions() -> Promise<Void> {


        return Promise{ fulfill, reject in


            let (_, _, _, acc_id) = MembershipHelper.getCurrentUserAccessing()

            if let _ovid = selectedOv["ovid"] as String!, _ov_type = selectedOv["type"] as String!, _oid = selectedToolbar["oid"] as String!, _mtype = selectedOv["mtype"] as String!{

                let params = [
                    "acc_id": acc_id,
                    "oid": _oid,
                    "mtype": _mtype,
                    "ov_type": _ov_type,
                    "ovid": _ovid
                ]
                self.listActionsView.alphaValue = 0.5;
                ToolbarActionHelper.getListToolbarActions_Promise(params).then{ listActions -> Void in
                    self.listActions = listActions;
                    self.listActionsView.reloadData()
                    if(self.listActions.count > 0){
                        self.listActionsView.performBatchUpdates({}, completionHandler: {
                            (finished: Bool) in
                            self.updateProgressBar("load Actions");
                            fulfill(())
                        })
                        
                    }
                    
                }
                self.listActionsView.alphaValue = 1;
                
                fulfill(())
                
            }

        }
    }
}









