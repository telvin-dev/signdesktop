
import Foundation
import SwiftyJSON

class ssMovie{
    var movieId: String!
    var title: String!
    var overview: String!
    var posterPath: String!
    
    init(movieJSON: JSON){
        if let title = movieJSON["name"].rawString() {
            self.title = title
        }
        
        if let overview = movieJSON["description"].rawString() {
            self.overview = overview
        }
        
        if let path = movieJSON["icon"].rawString(){
            self.posterPath = "\(path)"
        }
        
        if let movie_id = movieJSON["ovid"].rawString(){
            self.movieId = movie_id
        }
    }
    
}
