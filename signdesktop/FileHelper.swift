//
//  FileHelper.swift
//  signdesktop
//
//  Created by apple on 3/4/16.
//  Copyright © 2016 signsmart. All rights reserved.
//

import Foundation
import FileKit
import Alamofire

class FileHelper{
    
    static func getAlamofireDownloadDestination(
        path: String)
        -> Alamofire.Request.DownloadFileDestination
    {
        return { temporaryURL, response -> NSURL in
            return Path(path).URL
        }
    }

    
    static func mimeTypeForData(data: NSData) -> String {
        var c: UInt8 = 0
        data.getBytes(&c, length: 1)
        
        
        switch c {
        case 0xFF:
            return "image/jpeg"
        case 0x89:
            return "image/png"
        case 0x47:
            return "image/gif"
        case 0x49, 0x4D:
            return "image/tiff"
        case 0x25:
            return "application/pdf"
        case 0xD0:
            return "application/vnd"
        case 0x46:
            return "text/plain"
        default:
            return "application/octet-stream"
        }
    }
    
    // this function creates the required URLRequestConvertible and NSData we need to use Alamofire.upload
    static func urlRequestWithComponents(urlString:String, parameters:Dictionary<String, String>, path:Path) -> (URLRequestConvertible, NSData) {
        
        // create url request to send
        let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: urlString)!)
        
        // create upload data to send
        let uploadData = NSMutableData()
        
        do{
            let data = try NSData.readFromPath(path)
            let mime = self.mimeTypeForData(data);
            let ext = path.pathExtension
            
            
            mutableURLRequest.HTTPMethod = Alamofire.Method.POST.rawValue
            let boundaryConstant = "myRandomBoundary12345";
            let contentType = "multipart/form-data;boundary="+boundaryConstant
            mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
            mutableURLRequest.setValue("chunked", forHTTPHeaderField: "Transfer-Encoding")
            
            
            
            // add image
            uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            uploadData.appendData("Content-Disposition: form-data; name=\"file\"; filename=\"file.\(ext)\"\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            uploadData.appendData("Content-Type: \(mime)\r\n\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
            uploadData.appendData(data)
            
            // add parameters
            for (key, value) in parameters {
                uploadData.appendData("\r\n--\(boundaryConstant)\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
                uploadData.appendData("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n\(value)".dataUsingEncoding(NSUTF8StringEncoding)!)
            }
            uploadData.appendData("\r\n--\(boundaryConstant)--\r\n".dataUsingEncoding(NSUTF8StringEncoding)!)
        }
        catch _ {
        }
        
        
        
        // return URLRequestConvertible and NSData
        return (Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters: nil).0, uploadData)
    }
    
}