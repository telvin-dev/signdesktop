//
//  MovieCellCollectionViewCell.swift
//  ss-tvapp
//
//  Created by apple on 1/27/16.
//  Copyright © 2016 signsmart. All rights reserved.
//

import CocoaLumberjack
import SVGKit
import THLabel
import JAMSVGImage

class ToolbarCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var oid: String!
    var mtype: String!
    var toolbar_extension: String!
    
    
    var support_cids: [String!] = []
    var support_gids: [String!] = []
    var support_iids: [String!] = []
    
    var isSvg = false
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var name: THLabel!
    @IBOutlet weak var svgImageView: SVGKLayeredImageView!
    //@IBOutlet weak var svgImageView: UIImageView!
    
    
    func configureCell(toolbar: Toolbar){
        
        if let name = toolbar.name{
            self.name.text = name
        }
        
        
        if let path = toolbar.icon{
            
            /* this cannot handle SVG
            let url = NSURL(string: path)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)){
            let data = NSData(contentsOfURL: url!)
            
            dispatch_async(dispatch_get_main_queue()){
            let img = UIImage(data: data!)
            self.icon.image = img
            }
            }
            */
            
            //var imgView: UIView!
            if let url = NSURL(string: path){
                print(path)
                if let pathExtension = url.pathExtension as String?{
                    if pathExtension.lowercaseString == "svg"{
                        //print("svg type file")
                        self.isSvg = true;
                        
                        
                        if let svgImg = SVGKImage(contentsOfURL: url){
                            self.svgImageView.image = svgImg
                        }
                        
                        
                        self.svgImageView.hidden = false;
                        self.icon.hidden = true;
                        
                        self.layoutIcon(self.svgImageView)
                        print("===svg==image")
                        
                        
                        //svgImageView.image = SVGKImage(contentsOfURL: url)
                        //                        self.svgImageView.hidden = false;
                        //
                        //                        if let svgContent = JAMSVGImage(SVGData: NSData(contentsOfURL: url)){
                        //                            svgContent.drawInCurrentContext()
                        //                            self.svgImageView.svgImage = svgContent
                        //                        }
                        
                    }
                    else{
                        //imgView = self.icon
                        self.svgImageView.hidden = true
                        self.icon.hidden = false
                        self.icon.sd_setImageWithURL(url)
                        
                        self.layoutIcon(self.icon)
                        //print("===normal image thing")
                        
                    }
                    
                }
            }
            
        }
        
        if let oid = toolbar.oid{
            self.oid = oid
        }
        if let mtype = toolbar.mtype{
            self.mtype = mtype
        }
        if let oid = toolbar.oid{
            self.oid = oid
        }
        if let mtype = toolbar.mtype{
            self.mtype = mtype
        }
        
        if let toolbar_extension = toolbar.toolbar_extension{
            self.toolbar_extension = toolbar_extension
        }
        
        self.support_cids = toolbar.support_cids
        
        self.support_gids = toolbar.support_gids
        
        self.support_iids = toolbar.support_iids
        
        self.name.strokeColor = UIColor.blackColor()
        self.name.strokeSize = 2.0
        
        
    }
    
    func layoutIcon(icon: UIView!){
        
        
        icon.layer.cornerRadius = 10.0 // half of image size w:266 h:266
        icon.layer.borderColor = UIColor.grayColor().CGColor
        icon.layer.borderWidth = 2.0
        icon.layer.masksToBounds = true
    }
    
    override func didUpdateFocusInContext(context: UIFocusUpdateContext, withAnimationCoordinator coordinator: UIFocusAnimationCoordinator) {
        if (self.focused) {
            
            
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.layer.zPosition = 2
                if(self.isSvg){
                    
                    self.svgImageView.layer.cornerRadius = 12.0
                    self.svgImageView.frame.size = ContentBoxSize.Focus
                }else{
                    
                    self.icon.frame.size = ContentBoxSize.Focus
                    self.icon.layer.cornerRadius = 12.0
                    
                }
                
                if let collectionView = self.superview as? UICollectionView{
                    collectionView.collectionViewLayout.invalidateLayout();
                }
                
                
                EffectHelper.enableShadow(self, color: UIColor.whiteColor())
                
            })
            
        }
        else{
            UIView.animateWithDuration(0.1, animations: { () -> Void in
                self.layer.zPosition = 1
                
                if(self.isSvg){
                    
                    self.svgImageView.frame.size = ContentBoxSize.Default
                    self.svgImageView.layer.cornerRadius = 10.0
                }
                else{
                    
                    self.icon.frame.size = ContentBoxSize.Default
                    self.icon.layer.cornerRadius = 10.0
                }
                
                if let collectionView = self.superview as? UICollectionView{
                    collectionView.collectionViewLayout.invalidateLayout();
                }
                
                
                EffectHelper.enableShadow(self, color: nil)
                
            })
            
        }
        self.contentMode = .Center
        
    }
}
