//
//  PlayVideoController.swift
//  signdesktop


import Cocoa
import Alamofire
import SwiftyJSON
import FileKit
import AVKit
import AVFoundation



class PlayVideoController: NSViewController{
    
    
    @IBOutlet weak var avPlayerView: AVPlayerView!
    
      var ovid: String!
    
    override func viewDidLoad() {
        self.fetchVideo()
    }
    
    func playSSVideo(videoFileURL: String){
        let player = AVPlayer(URL: NSURL(string: videoFileURL)!)
        self.avPlayerView.player = player
        self.avPlayerView.showsFullScreenToggleButton = true;

        player.play()
        //repeat video
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"playerItemDidReachEnd:", name: AVPlayerItemDidPlayToEndTimeNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector:"WindowWillClose:", name: NSWindowWillCloseNotification, object: nil)

    }
    


func fetchVideo(){
    let getVideoInfoUrl = CommonHelper.makeApiUrl("/tvos/getvideo");
    let(_,_,_,acc_id) = MembershipHelper.getCurrentUserAccessing()
    if let _ovid = self.ovid {
        
        let params = [
            "acc_id": acc_id!,
            "ovid": _ovid
        ]
        
        Alamofire.request(.POST, getVideoInfoUrl, parameters: params)
            .responseJSON { response in
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        if let json: JSON? = JSON(value){
                            if json!["OK"].number!.integerValue == 1{
                                if json!["videoType"].number == SignSmartVideoType.SIGNSMART{
                                
                                    self.playSSVideo(json!["content"].rawString()!)
                                }
                            }
                        }

                    }
                case .Failure(let error):
                    print(error)
                }
        }
        
    }
    
}

    @objc func playerItemDidReachEnd(notification: NSNotification){
        self.avPlayerView.player!.seekToTime(kCMTimeZero)
        self.avPlayerView.player!.play()
    }

    @objc func WindowWillClose(notification: NSNotification){
        self.avPlayerView.player!.pause()
        self.avPlayerView = nil
        NSNotificationCenter.defaultCenter().removeObserver(self, name: AVPlayerItemDidPlayToEndTimeNotification, object:nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: NSWindowWillCloseNotification, object:nil)

    }

}







