//
//  ViewController.swift
//  signdesktop

import Cocoa
import Alamofire
import SwiftyJSON

class LoginController: NSViewController, MBProgressHUDDelegate, NSApplicationDelegate {
    
    @IBOutlet weak var userNameTextField: NSTextField!
    
    @IBOutlet weak var passwdTextField: NSTextField!
    @IBOutlet weak var errorLabel: NSTextField!
    @IBOutlet weak var loginBtn: NSButton!
    
    var loader: MBProgressHUD!;    
    
    override func viewWillAppear() {
        
        
        NSApplication.sharedApplication().presentationOptions = [NSApplicationPresentationOptions.HideMenuBar , NSApplicationPresentationOptions.HideDock]

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredContentSize = NSSize(width: 640.0, height: 260.0)
        userNameTextField.becomeFirstResponder()
        self.loader = ViewHelper.addHUD(self, viewDelegate: self)
        loader.hide(true)
    }
    
    @IBAction func loginClick(sender: NSButton) {
        if(userNameTextField.stringValue == "" || passwdTextField.stringValue == ""){
            self.errorLabel.stringValue = "User name and password are required fields."
            self.errorLabel.hidden = false;
            return;
        }
        
        
        
        switchGui(true)
        
        let loginUrl = CommonHelper.makeApiUrl("/home/login");
        let params = [
            "login_name": userNameTextField.stringValue,
            "password": passwdTextField.stringValue
        ]
        
        Alamofire.request(.POST, loginUrl, parameters: params)
            .responseJSON { response in
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        if let json: JSON? = JSON(value){
                            if json!["OK"].number!.integerValue == 1{
                                // join in Apple TV account if not
                                let defaults = NSUserDefaults.standardUserDefaults()
                                defaults.setObject(json!["id_token"].string, forKey: "signsmartUserToken")
                                
                                defaults.setObject(json!["member_id"].string, forKey: "signsmartUserId")
                                defaults.setObject(json!["member_id"].string, forKey: "signsmartLoginId")
                                defaults.setObject(UserType.MEMBER, forKey: "signsmartUserType")
                                defaults.setObject(self.userNameTextField.stringValue, forKey: "signsmartUserName")
                                self.errorLabel.hidden = true;
                                NSApplication.sharedApplication().mainWindow?.close() //close current window first
                                self.performSegueWithIdentifier("GotoViewControllerSegue", sender: self) // open the second window


                            }else{
                                self.switchGui(false)
                                self.errorLabel.hidden = false;
                                self.errorLabel.stringValue = json!["errors"].string!
                            }
                        }else{
                            self.switchGui(false)
                        }
                        
                    }
                case .Failure( _):
                    self.errorLabel.stringValue = "Due to network issue, the data cannot be transfered thoroughly."
                     self.errorLabel.hidden = false;
                    self.switchGui(false)
                    break;
                }
        }
        
        
        
    }
    
    func switchGui(inProcess: Bool){
        loginBtn.enabled = !inProcess;
        if(inProcess){
            loginBtn.alphaValue = 0.3
            loader.show(true)
        }else{
            loginBtn.alphaValue = 1
            loader.hide(true)
        }
        
    }

    override var representedObject: AnyObject? {
        didSet {
            // Update the view, if already loaded.
        }
    }

}


