import Foundation
import AppKit
import CocoaLumberjack
import SVGKit
import ProgressKit
import OTWebImage

class _UserCellCVI: NSCollectionViewItem{


    @IBOutlet weak var icon: NSImageView!
    @IBOutlet weak var svgIcon: SVGKLayeredImageView!


    var user:User? {
        return super.representedObject as? User
    }

    override var representedObject: AnyObject? {
        get {
            return super.representedObject
        }
        set(newRepresentedObject) {
            if(newRepresentedObject != nil){
                super.representedObject = newRepresentedObject
                configureCell(self.user!)
            }
        }
    }

    override var selected: Bool {
        didSet {
            (self.view as! _UserCellView).selected = selected
        }
    }
    override var highlightState: NSCollectionViewItemHighlightState {
        didSet {
            (self.view as! _UserCellView).highlightState = highlightState
        }
    }

    // MARK: NSResponder

    override func mouseDown(theEvent: NSEvent) {
        if theEvent.clickCount == 2 {
        } else {
            super.mouseDown(theEvent)
        }

    }

    func configureCell(user: User){

        if let path = user.icon{

            if let url = NSURL(string: path){
                if let pathExtension = url.pathExtension as String?{
                    if pathExtension.lowercaseString == "svg"{

                        self.svgIcon.image = SVGKImage(contentsOfURL: url)
                        self.svgIcon.hidden = false;
                        self.layoutIcon(self.svgIcon)
                    }
                    else{

                        self.svgIcon.hidden = true;
                        self.icon.image = NSImage(named: url.URLByDeletingPathExtension!.lastPathComponent!)
                        if((self.icon.image == nil)){
                            self.icon.image = NSImage.init(contentsOfURL: url)
                        }
                        self.icon.imageURL = url
                        self.layoutIcon(self.icon)

                    }
                }
            }
        }
    }

    func layoutIcon(icon: NSView){
        icon.layer!.cornerRadius = 32.0
        icon.layer!.borderWidth = 2.0
        icon.layer!.masksToBounds = true

    }
    
    
}