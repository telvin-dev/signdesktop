import Cocoa
import FileKit
import Alamofire
import SVGKit
import PromiseKit


class ContentController: NSViewController, NSCollectionViewDataSource, NSCollectionViewDelegate{


    @IBOutlet weak var listUserAccountsView: NSCollectionView!
    @IBOutlet weak var listToolbarCollectionView: NSCollectionView!

    @IBOutlet weak var listCvsView: NSCollectionView!
    @IBOutlet weak var listGvsView: NSCollectionView!
    @IBOutlet weak var listIvsView: NSCollectionView!

    @IBOutlet weak var listActionsView: NSCollectionView!

    @IBOutlet weak var loadingBoxView: LoadingBox!

    @IBOutlet weak var btnLeftScroll_Item: NSButton!
    @IBOutlet weak var btnRightScroll_Item: NSButton!


    @IBOutlet weak var btnLeftScroll_Group: NSButton!
    @IBOutlet weak var btnRightScroll_Group: NSButton!

    @IBOutlet weak var btnLeftScroll_Collection: NSButton!
    @IBOutlet weak var btnRightScroll_Collection: NSButton!


    var appWindowController: NSWindowController!

    var listUserAccounts = [User]();
    var listToolbarCollection = [Toolbar]();
    var listToolbarGroup = [Toolbar]();
    var listToolbarItem = [Toolbar]();

    var listCvs = [ObjectVariant]();
    var listGvs = [ObjectVariant]();
    var listIvs = [ObjectVariant]();

    var listActions = [ToolbarAction]();

    var support_cids = [String!]();
    var support_gids = [String!]();
    var support_iids = [String!]();

    var selectedToolbar = [
        "oid"               : "6",
        "mtype"             : "Item",
        "toolbar_extension" : ""
    ]

    var selectedOv = [
        "ovid": "",
        "type": "",
        "mtype": ""
    ];

    var ivPageIndex = 1
    var ivPageCount = 0

    var gvPageIndex = 1
    var gvPageCount = 0

    var cvPageIndex = 1
    var cvPageCount = 0

    weak var request: Alamofire.Request?


    override func viewDidLoad() {

        loadingBoxView.contentHolder = self
        super.viewDidLoad()

    }

    var tasks:[String:Double]!

    override func viewWillAppear() {
        super.viewWillAppear()

        NSApplication.sharedApplication().presentationOptions = NSApplicationPresentationOptions.Default
        self.preferredContentSize = NSSize(width: 1400.0, height: 820.0)


        listUserAccountsView.dataSource = self
        listUserAccountsView.delegate = self
        listToolbarCollectionView.dataSource = self
        listToolbarCollectionView.delegate = self

        listCvsView.dataSource = self
        listCvsView.delegate = self

        listGvsView.dataSource = self
        listGvsView.delegate = self

        listIvsView.dataSource = self
        listIvsView.delegate = self

        listActionsView.dataSource = self
        listActionsView.delegate = self



        self.tasks = [
            "loadUserAccounts": 50,
            "loadToolbars_Collection": 50,
        ]
        self.loadingBoxView.start();

        self.loadUserAccounts().then { Void in

            self.loadToolbars_Collection()
        }


    }

    func updateProgressBar(key: String){
        if let percent = self.tasks[key]{
            self.loadingBoxView.progressBar.doubleValue += percent
            self.tasks.removeValueForKey(key)
            if(self.tasks.isEmpty){
                self.loadingBoxView.progressBar.doubleValue = 100.0;
                self.loadingBoxView.finish();
            }

        }
    }


    func loadUserAccounts() -> Promise<Void> {

        return Promise{ fulfill, reject in

            self.listUserAccountsView.alphaValue = 0.5;


            MembershipHelper.getListUserAvatars_Promise().then{ listUsers -> Void in


                self.listUserAccounts = listUsers;
                self.listUserAccountsView.reloadData()

                // toggle member - first item
                let index = NSIndexPath(forItem: 0, inSection: 0)
                var set = Set<NSIndexPath>()
                set.insert(index)
                self.listUserAccountsView.selectItemsAtIndexPaths(set, scrollPosition: NSCollectionViewScrollPosition.Top)
                if(listUsers.count > 0){



                    self.listUserAccountsView.performBatchUpdates({}, completionHandler: {
                        (finished: Bool) in

                        self.updateProgressBar("loadUserAccounts");
                        fulfill(())
                    })

                    self.listUserAccountsView.alphaValue = 1;


                }

                else{
                    self.updateProgressBar("loadUserAccounts");
                    fulfill(())
                }


                }
                .error{ Void in
                    self.loadingBoxView.hasProblem()
            }


        }
    }


    func collectionView(collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView == self.listUserAccountsView) {
            return listUserAccounts.count
        }
        else if (collectionView == self.listToolbarCollectionView) {
            return listToolbarCollection.count
        }
        else if (collectionView == self.listCvsView) {
            return listCvs.count
        }
        else if (collectionView == self.listGvsView) {
            return listGvs.count
        }
        else if (collectionView == self.listIvsView) {
            return listIvs.count
        }
        else if (collectionView == self.listActionsView) {
            return listActions.count
        }

        return 0;
    }

    func collectionView(collectionView: NSCollectionView, itemForRepresentedObjectAtIndexPath indexPath: NSIndexPath) -> NSCollectionViewItem {
        if (collectionView == self.listUserAccountsView) {
            let cell = collectionView.makeItemWithIdentifier("UserCellXib", forIndexPath: indexPath)

            var user: User
            user = listUserAccounts[indexPath.item]
            cell.representedObject = user;

            return cell
        }

        else if (collectionView == self.listToolbarCollectionView) {
            let cell = collectionView.makeItemWithIdentifier("ToolbarCellXib", forIndexPath: indexPath)

            var toolbar: Toolbar
            toolbar = listToolbarCollection[indexPath.item]
            cell.representedObject = toolbar;
            return cell
        }
        else if (collectionView == self.listCvsView) {
            let cell = collectionView.makeItemWithIdentifier("ObjectVariantXib", forIndexPath: indexPath)

            var ov: ObjectVariant
            ov = listCvs[indexPath.item]
            cell.representedObject = ov;
            return cell
        }
        else if (collectionView == self.listGvsView) {
            let cell = collectionView.makeItemWithIdentifier("ObjectVariantXib", forIndexPath: indexPath)

            var ov: ObjectVariant
            ov = listGvs[indexPath.item]
            cell.representedObject = ov;
            return cell
        }


        else if (collectionView == self.listIvsView) {
            let cell = collectionView.makeItemWithIdentifier("ObjectVariantXib", forIndexPath: indexPath)

            var ov: ObjectVariant
            ov = listIvs[indexPath.item]
            cell.representedObject = ov;
            return cell
        }

        else if (collectionView == self.listActionsView) {
            let cell = collectionView.makeItemWithIdentifier("ToolbarActionXib", forIndexPath: indexPath)

            var ov: ToolbarAction
            ov = listActions[indexPath.item]
            cell.representedObject = ov;
            return cell
        }



        return NSCollectionViewItem()
    }

    func collectionView(collectionView: NSCollectionView, didSelectItemsAtIndexPaths indexPaths: Set<NSIndexPath>) {

        if (collectionView == self.listUserAccountsView) {
            let cell = self.listUserAccountsView.itemAtIndexPath(indexPaths.first!) as! _UserCellCVI
            if let data = cell.representedObject as? User{

                NSUserDefaults.standardUserDefaults().setObject(data.userType, forKey: "signsmartUserType")
                NSUserDefaults.standardUserDefaults().setObject(data.userId, forKey: "signsmartUserId")

                cleanListCv()
                cleanListGv()
                cleanListIv()
            }

        }
        else if (collectionView == self.listToolbarCollectionView) {
            let cell = self.listToolbarCollectionView.itemAtIndexPath(indexPaths.first!) as! _ToolbarCellCVI

            if let data = cell.representedObject as? Toolbar{


                self.selectedToolbar["oid"] = data.oid
                self.selectedToolbar["mtype"] = data.mtype
                self.selectedToolbar["toolbar_extension"] = data.toolbar_extension

                self.selectedOv["mtype"] = data.mtype


                if(data.mtype == ToolbarKind.COLLECTION){
                    self.support_cids = [data.oid]
                    self.support_gids = data.support_gids
                    self.support_iids = data.support_iids


                }
                else if(data.mtype == ToolbarKind.GROUP){
                    self.support_cids = data.support_cids
                    self.support_gids = [data.oid]
                    self.support_iids = data.support_iids

                }
                else if(data.mtype == ToolbarKind.ITEM){
                    self.support_cids = data.support_cids
                    self.support_gids = data.support_gids
                    self.support_iids = [data.oid]

                }

                self.selectedOv["ovid"] = ""
                self.selectedOv["type"] = ""

                self.loadAllVariants();
            }

        }

        else if (collectionView == self.listCvsView) {
            let cell = self.listCvsView.itemAtIndexPath(indexPaths.first!) as! _ObjectVariantCVI

            if let data = cell.representedObject as? ObjectVariant{

                self.selectedOv["ovid"] = data.ovid
                self.selectedOv["type"] = data.type
                self.selectedOv["mtype"] = data.mtype


            }
            self.loadVariants_Group()
            self.loadVariants_Item()
            self.loadToolbarActions();
        }

        else if (collectionView == self.listGvsView) {
            let cell = self.listGvsView.itemAtIndexPath(indexPaths.first!) as! _ObjectVariantCVI

            if let data = cell.representedObject as? ObjectVariant{
                self.selectedOv["ovid"] = data.ovid
                self.selectedOv["type"] = data.type
                self.selectedOv["mtype"] = data.mtype


            }
            self.loadVariants_Collection()
            self.loadVariants_Item()
            self.loadToolbarActions();

        }


        else if (collectionView == self.listIvsView) {

            let cell = self.listIvsView.itemAtIndexPath(indexPaths.first!) as! _ObjectVariantCVI

            if let data = cell.representedObject as? ObjectVariant{
                self.selectedOv["ovid"] = data.ovid
                self.selectedOv["type"] = data.type
                self.selectedOv["mtype"] = data.mtype


            }
            self.loadVariants_Collection()
            self.loadVariants_Group()
            self.loadToolbarActions();
        }

        else if (collectionView == self.listActionsView) {
            let cell = self.listActionsView.itemAtIndexPath(indexPaths.first!) as! _ToolbarActionCVI
            if let data = cell.representedObject as? ToolbarAction{
                let storyboard = NSStoryboard(name: "Main", bundle: nil)
                if(self.selectedToolbar["toolbar_extension"]! == SignSmartExtension.Video){
                    if(data.exec_function == "Playvideo"){
                        let nav = storyboard.instantiateControllerWithIdentifier("PlayVideoController") as? PlayVideoController
                        nav!.ovid = self.selectedOv["ovid"]

                        let windowStyleMask: Int = NSTitledWindowMask|NSResizableWindowMask|NSMiniaturizableWindowMask|NSClosableWindowMask
                        let playVideoWindow: NSWindow = NSWindow(contentRect: NSMakeRect(NSScreen.mainScreen()!.frame.width/2,  NSScreen.mainScreen()!.frame.height/2, NSScreen.mainScreen()!.frame.width/2, NSScreen.mainScreen()!.frame.height/2), styleMask: windowStyleMask, backing: NSBackingStoreType.Buffered, `defer`: true)
                        playVideoWindow.backgroundColor = NSColor.lightGrayColor()
                        playVideoWindow.minSize = NSMakeSize(1280, 720)
                        playVideoWindow.makeKeyAndOrderFront(self)
                        self.appWindowController = NSWindowController(window: playVideoWindow)
                        self.appWindowController.contentViewController = nav
                        self.appWindowController.showWindow(self)

                    }
                }
                else if(self.selectedToolbar["toolbar_extension"]! == SignSmartExtension.Bookmark){
                    if(data.exec_function == "RunObject"){
                        ViewBookmarkController(ovid: self.selectedOv["ovid"]!)
                    }
                }
                else if(data.exec_function == "ViewObject"){
                    if(self.selectedToolbar["toolbar_extension"]! == SignSmartExtension.Image){
                        let nav = storyboard.instantiateControllerWithIdentifier("ViewImageController") as! ViewImageController
                        nav.ovid = self.selectedOv["ovid"]
                        let screenSize = NSScreen.mainScreen()?.frame
                        nav.view.frame = CGRectMake(0.0, 0.0, screenSize!.width,screenSize!.height)
                        let windowStyleMask: Int = NSTitledWindowMask|NSResizableWindowMask|NSMiniaturizableWindowMask|NSClosableWindowMask
                        let viewImageWindow: NSWindow = NSWindow(contentRect: NSMakeRect(0,0,screenSize!.width,screenSize!.height), styleMask: windowStyleMask, backing: NSBackingStoreType.Buffered, defer: true)
                        viewImageWindow.backgroundColor = NSColor.lightGrayColor()
                        viewImageWindow.center()

                        viewImageWindow.makeKeyAndOrderFront(self)
                        self.appWindowController = NSWindowController(window: viewImageWindow)
                        self.appWindowController.contentViewController = nav
                        self.appWindowController.showWindow(self)
                    }
                    else if(self.selectedToolbar["toolbar_extension"]! == SignSmartExtension.Document){
                        ViewDocumentController(ovid: self.selectedOv["ovid"]!)
                    }
                }
                else if(data.exec_function == "CreateObject"){
                    if(self.selectedToolbar["toolbar_extension"]! == SignSmartExtension.Video || self.selectedToolbar["toolbar_extension"]! == SignSmartExtension.Image || self.selectedToolbar["toolbar_extension"]! == SignSmartExtension.Document){
                        let nav = storyboard.instantiateControllerWithIdentifier("UploadVideoController") as! UploadVideoController
                        nav.oid = self.selectedToolbar["oid"]
                        nav.parentVc = self

                        let windowStyleMask: Int = NSTitledWindowMask|NSResizableWindowMask|NSMiniaturizableWindowMask|NSClosableWindowMask
                        let uploadVideoWindow: NSWindow = NSWindow(contentRect: NSMakeRect(NSScreen.mainScreen()!.frame.width/2,  NSScreen.mainScreen()!.frame.height/2, NSScreen.mainScreen()!.frame.width/2, NSScreen.mainScreen()!.frame.height/2), styleMask: windowStyleMask, backing: NSBackingStoreType.Buffered, defer: true)
                        uploadVideoWindow.backgroundColor = NSColor.lightGrayColor()
                        uploadVideoWindow.minSize = NSMakeSize(1280, 720)
                        uploadVideoWindow.center()
                        uploadVideoWindow.makeKeyAndOrderFront(self)
                        self.appWindowController = NSWindowController(window: uploadVideoWindow)
                        self.appWindowController.contentViewController = nav
                        self.appWindowController.showWindow(self)

                    }

                }


            }
        }
    }

    func loadAllVariants(){
        self.tasks = [
            "loadVariants_Collection": 25,
            "loadVariants_Group": 25,
            "loadVariants_Item": 50,
        ]
        
        self.loadingBoxView.start();
        
        self.loadVariants_Collection().then{ Void in
            self.loadVariants_Group().then{ Void in
                self.loadVariants_Item().then{ Void in
                    self.loadToolbarActions()
                }
            }
        }
        
    }
    
    func toggleObjectVariant(type: String){
        var cell = NSCollectionViewItem()
        let index = NSIndexPath(forItem: 0, inSection: 0) // get first item
        var set = Set<NSIndexPath>()
        set.insert(index)
        
        switch type {
            
        case "cv":
            
            cell = listCvsView!.itemAtIndex(0)!
        case "gv":
            
            cell = listGvsView!.itemAtIndex(0)!
        case "iv":
            cell = listIvsView!.itemAtIndex(0)!
            if let data = cell.representedObject as? ObjectVariant{
                self.selectedOv["ovid"] = data.ovid
                self.selectedOv["type"] = data.type
                self.selectedOv["mtype"] = data.mtype
                listIvsView.selectItemsAtIndexPaths(set, scrollPosition: NSCollectionViewScrollPosition.Top)
            }
            
        default:
            break
        }
        self.loadToolbarActions()
    }

}


