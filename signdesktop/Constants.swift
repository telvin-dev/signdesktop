
import Foundation
struct Constant {
    static var API_HOST = "https://api.themoviedb.org/3"
    static var API_KEY = "eaca320bbadd046c56928fe5a75a5652"
    
    static var SS_API_HOST = "https://signsmart.com/prod";
    static var UNLIMIT = 1000000
    
    static var APPLE_TV_ACC_ID = "1424"
    static var APPLE_TV_TRAILER_GV_ID = "17382"
}

struct ToolbarKind{
    static var COLLECTION = "Collection"
    static var GROUP = "Group"
    static var ITEM = "Item"
}

struct VariantKind{
    static var COLLECTION = "cv"
    static var GROUP = "gv"
    static var ITEM = "iv"
}


struct UserType{
    static var MEMBER = "member_id"
    static var ACCOUNT = "account_id"
}

struct ListMovieKind {
    static var Trailer = 1
    static var SignsmartMovie = 2
    
}

struct ContentType{
    static var UserAccount = 0
    static var Toolbar = 2
    static var CollectionVariant = 4
    static var GroupVariant = 6
    static var ItemVariant = 8
    static var ToolbarAction = 10

}

struct SignSmartVideoType{
    static var SIGNSMART = 1
    static var YOUTUBE = 2

}

struct ContentBoxSize{
    static var Default = CGSizeMake(152, 152)
    static var Focus = CGSizeMake(167, 167)
}


struct SignSmartExtension{
    static var Bookmark = ".bookmark"
    static var Url = ".url"
    static var Video = ".video"
    static var Image = ".image"
    static var Music = "..music"
    static var Html = ".html"
    static var Document = ".document"
    static var YoutubeVideo = "app-tv.youtube-video"
    
}