
import Foundation
import Cocoa
import HudOSXSwift

struct ViewHelper{

    static func backgroundColor(view: NSView, color: NSColor) {
        view.wantsLayer = true
        view.layer?.backgroundColor = color.CGColor
    }

    static func addHUD(vc: NSViewController, viewDelegate: MBProgressHUDDelegate) -> MBProgressHUD{
        
        let HUD = MBProgressHUD(view: vc.view)
        HUD.mode = MBProgressHUDModeIndeterminate
        vc.view.addSubview(HUD)
        HUD.delegate = viewDelegate
        HUD.minSize = CGSizeMake(135.0, 135.0)
        return HUD

    }
    
    static func removeHUD(hud: MBProgressHUD) -> Void{
        hud.hide(true) // hidden also release it
    }


}
