
import Foundation
import Alamofire
import SwiftyJSON
import PromiseKit

class ToolbarActionHelper{
    static func getListToolbarActions_Promise(params: NSDictionary)->Promise<[ToolbarAction]>{
        
        return Promise{ fulfil, reject in
            
            var list = [ToolbarAction]()
            
            
            
            let fetchDataUrl = CommonHelper.makeApiUrl("/osx/gettoolbaractions");
            
            
            Alamofire.request(.POST, fetchDataUrl, parameters: params as? [String : AnyObject])
                .responseJSON {
                    response in
                    
                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            if let   json: JSON? = JSON(value){
                                if json!["OK"].number!.integerValue == 1{
                                    if let jsonList: JSON? = json!["actions"]{
                                        for (_,entry):(String, JSON) in jsonList! {
                                            let action = ToolbarAction(actionJSON: entry)
                                            list.append(action)
                                        }
                                    }
                                    
                                    fulfil(list)
                                }else{
                                  
                                }
                                
                            }
                            
                        }
                    case .Failure(let error):
                        reject(error)
                    }
            }
            
        }
        
    }

}
