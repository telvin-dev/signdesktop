//
//  File.swift
//  signdesktop
//
//  Created by apple on 3/29/16.
//  Copyright © 2016 signsmart. All rights reserved.
//

import Cocoa

class NSColorView: NSView {
    var backgroundColor: NSColor {
        get {
            return self.backgroundColor
        }
        set {
            self.needsDisplay = true
        }
    }
    
    
    override func updateLayer() {
        self.layer!.backgroundColor = self.backgroundColor.CGColor
    }
}