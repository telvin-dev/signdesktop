import Cocoa
import FileKit
import Alamofire
import SVGKit
import PromiseKit


extension ContentController {
    
    func loadVariants_Collection() -> Promise<Void> {
        return Promise{ fulfill, reject in
            
            self.listCvsView.alphaValue = 0.5;
            let (_, _, _, acc_id) = MembershipHelper.getCurrentUserAccessing()
            let list_oids = self.support_cids //
            
            if let _ovid = selectedOv["ovid"] as String!, _ov_type = selectedOv["type"] as String!{
                
                let params = [
                    "acc_id": acc_id,
                    "list_type": "collection",
                    "list_oid": list_oids,
                    "pageIndex": self.cvPageIndex,
                    "sort_order": "desc",
                    "sorter": "cv_id",
                    "keyword": "",
                    "mtype": "Collection",
                    "pageSize": 10, //Constant.UNLIMIT,
                    "ov_type": _ov_type,
                    "ovid": _ovid
                ]
                
                
                
                VariantHelper.getListVariants_Promise(params).then { data -> Void in

                    self.listCvs = data["list"] as! [ObjectVariant] ;
                    self.cvPageCount = data["pageCount"] as! Int

                    VariantHelper.autoShowScrollBtn(self.cvPageIndex, pageCount: self.cvPageCount, leftScroll: self.btnLeftScroll_Collection, rightScroll: self.btnRightScroll_Collection)
                    

                    self.listCvsView.reloadData()
                    if(self.listCvs.count > 0){
                        
                        self.listCvsView.performBatchUpdates({}, completionHandler: { (finished: Bool) in
                            self.updateProgressBar("loadVariants_Collection");
                            fulfill(())
                        })
                    }
                    else{
                        self.updateProgressBar("loadVariants_Collection");
                        fulfill(())
                    }
                    self.listCvsView.alphaValue = 1;
                }
                .error{ Void in
                    self.loadingBoxView.hasProblem() // show message & retry button
                }
                
            }
            
        }
        
    }
    
    func loadVariants_Group() -> Promise<Void> {
        
        // reset selected pointer
        return Promise{ fulfill, reject in
            
            self.listGvsView.alphaValue = 0.5;
            let (_, _, _, acc_id) = MembershipHelper.getCurrentUserAccessing()
            let list_oids = self.support_gids//[12]
            
            if let _ovid = selectedOv["ovid"] as String!, _ov_type = selectedOv["type"] as String!{
                
                let params = [
                    "acc_id": acc_id,
                    "list_type": "group",
                    "list_oid": list_oids,
                    "pageIndex": self.gvPageIndex,
                    "sort_order": "desc",
                    "sorter": "gv_id",
                    "keyword": "",
                    "mtype": "Group",
                    "pageSize": 10, //Constant.UNLIMIT,
                    "ov_type": _ov_type,
                    "ovid": _ovid
                ]
                VariantHelper.getListVariants_Promise(params).then { data -> Void in
                    self.listGvs = data["list"] as! [ObjectVariant];
                    self.gvPageCount = data["pageCount"] as! Int

                    VariantHelper.autoShowScrollBtn(self.gvPageIndex, pageCount: self.gvPageCount, leftScroll: self.btnLeftScroll_Group, rightScroll: self.btnRightScroll_Group)

                    self.listGvsView.reloadData()

                    if(self.listGvs.count > 0){
                        
                        self.listGvsView.performBatchUpdates({}, completionHandler: { (finished: Bool) in
                                self.updateProgressBar("loadVariants_Group");
                            
                            fulfill(())
                            
                        })
                    }
                    else{
                        self.updateProgressBar("loadVariants_Group");
                        fulfill(())
                    }
                    self.listGvsView.alphaValue = 1;
                }
                .error{ Void in
                        self.loadingBoxView.hasProblem() // show message & retry button
                }
                
            }
            
        }
        
    }
    
    func loadVariants_Item() -> Promise<Void> {
        return Promise{ fulfill, reject in
            
            self.listIvsView.alphaValue = 0.5;
            let (_, _, _, acc_id) = MembershipHelper.getCurrentUserAccessing()
            let list_oids = self.support_iids// [6]
            if let _ovid = selectedOv["ovid"] as String!, _ov_type = selectedOv["type"] as String!{

                let params = [
                    "acc_id": acc_id,
                    "list_type": "item",
                    "list_oid": list_oids,
                    "pageIndex": self.ivPageIndex,
                    "sort_order": "desc",
                    "sorter": "iv_id",
                    "keyword": "",
                    "mtype": "Item",
                    "pageSize": 10, //Constant.UNLIMIT,
                    "ov_type": _ov_type,
                    "ovid": _ovid
                ]

                VariantHelper.getListVariants_Promise(params).then { data -> Void in

                    self.listIvs = data["list"] as! [ObjectVariant]
                    self.ivPageCount = data["pageCount"] as! Int
                    VariantHelper.autoShowScrollBtn(self.ivPageIndex, pageCount: self.ivPageCount, leftScroll: self.btnLeftScroll_Item, rightScroll: self.btnRightScroll_Item)
                    self.listIvsView.reloadData()
                    if(self.listIvs.count > 0){
                        self.listIvsView.performBatchUpdates({}, completionHandler: { (finished: Bool) in
                            self.updateProgressBar("loadVariants_Item");
                            fulfill(())
                            
                        })
                    }
                    else{
                        self.updateProgressBar("loadVariants_Item");
                        fulfill(())
                    }
                    self.listIvsView.alphaValue = 1;
                }
                .error{ Void in
                        self.loadingBoxView.hasProblem() // show message & retry button
                }
                
            }
            
        }
        
    }
    
    func cleanListCv(){
        self.listCvs = [];
        self.listCvsView.reloadData()
        self.listCvsView.needsDisplay = true;

    }
    
    func cleanListGv(){
        self.listGvs = [];
        self.listGvsView.reloadData()
        self.listGvsView.needsDisplay = true;

    }
    
    func cleanListIv(){
        self.listIvs = [];
        self.listIvsView.reloadData()
        self.listIvsView.needsDisplay = true;
    }


    
    
}
