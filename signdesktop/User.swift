
import Foundation
import SwiftyJSON

class User{
    var userId: String!
    var userType: String!
    var name: String!
    var icon: String!
    
    init(user: JSON, userType: String!){
        if userType == UserType.MEMBER {
            if let userId = user["member_id"].rawString(){
                self.userId = userId
            }

            
            if let name = user["name"].rawString() {
                self.name = name
            }
            
        }
        else if userType == UserType.ACCOUNT{
            if let userId = user["account_id"].rawString() {
                self.userId = userId
            }
            
            if let name = user["company"].rawString() {
                self.name = name
            }
            
        }

        self.userType = userType
        
        
        if let icon = user["avatar"].rawString() {
            self.icon = icon
        }
    }
    
    
    init(movieId: NSInteger){
        
    }
    
}
