
import Foundation
import SwiftyJSON

class ObjectVariant {
    
    var ovid: String!
    var type: String!
    var name: String!
    var icon: String!
    var mtype: String!
    var oid: String!
    var toolbar_name: String!
    var toolbar_extension: String!
    var extVideo: String!
    
    init(ov: JSON){
        if let ovid = ov["ovid"].rawString(){
            self.ovid = ovid
        }
        
        if let type = ov["type"].rawString(){
            self.type = type
        }
        
        if let name = ov["name"].rawString() {
            self.name = name
        }
        
        
        if let icon = ov["icon"].rawString() {
            self.icon = icon
        }
        
        if let mtype = ov["mtype"].rawString(){
            self.mtype = mtype
        }
        
        
        if let oid = ov["oid"].rawString(){
            self.oid = oid
        }
        
        if let toolbar_name = ov["toolbar_name"].rawString(){
            self.toolbar_name = toolbar_name
        }
        
        if let toolbar_extension = ov["ext"].rawString(){
            self.toolbar_extension = toolbar_extension
        }
        if let extVideo = ov["extVideo"].rawString(){
            self.extVideo = extVideo
        }
        
    }
    
    init(){
        // empty method for subclass usage
    }
    
}
