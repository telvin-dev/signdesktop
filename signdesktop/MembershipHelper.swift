
import Foundation
import Alamofire
import SwiftyJSON
import PromiseKit


struct MembershipHelper{

    static func authenticate_Promise() -> Promise<Bool> {
        return Promise{ fulfill, reject in

            let loginUrl = CommonHelper.makeApiUrl("/home/authenticate");
            let params = [:]

            Alamofire.request(.POST, loginUrl, parameters: params as? Dictionary<String, AnyObject>)
                .responseJSON { response in

                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            var validated = false

                            if let deviceToken = NSUserDefaults.standardUserDefaults().stringForKey("signsmartUserToken"){
                                if let json: JSON? = JSON(value){
                                    if json!["serverCookie"].string == deviceToken{
                                        validated = true
                                    }

                                }
                            }
                            fulfill(validated)
                        }
                    case .Failure(let error):
                        reject(error)
                    }


            }

        }
    }

    /* determine whether member or account */
    static func getCurrentUserAccessing() -> (String!, String!, String!, String!){
        var userType = ""
        if let savedUserType = NSUserDefaults.standardUserDefaults().stringForKey("signsmartUserType"){
            userType = savedUserType
        }
        let userId = NSUserDefaults.standardUserDefaults().stringForKey("signsmartUserId")

        var member_id = ""
        var account_id = ""
        if userType == UserType.MEMBER{
            member_id = userId!
        }

        else if userType == UserType.ACCOUNT{
            account_id = userId!
        }

        else{
            // this is a guest, no login yet
            account_id = Constant.APPLE_TV_ACC_ID
            userType = UserType.ACCOUNT

        }



        return (userType, userId, member_id, account_id)
    }

    static func getListUserAvatars_Promise()->Promise<[User]>{

        return Promise{ fulfil, reject in

            var listUser = [User]()
            let fetchDataUrl = CommonHelper.makeApiUrl("/tvos/getavatarnav");

            Alamofire.request(.GET, fetchDataUrl)
                .responseJSON { response in

                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            if let json: JSON? = JSON(value){

                                if json!["OK"].number!.integerValue == 1{
                                    if let memberInfo: JSON? = json!["memberInfo"]{
                                        if(memberInfo?.count > 0){
                                            let user = User(user: memberInfo!, userType: UserType.MEMBER)
                                            listUser.append(user)

                                        }
                                    }

                                    if let accountsInfo: JSON? = json!["accountsInfo"]{
                                        for (_,eachUser):(String, JSON) in accountsInfo! {
                                            let user = User(user: eachUser, userType: UserType.ACCOUNT)
                                            listUser.append(user)
                                        }
                                    }

                                    fulfil(listUser)

                                }else{

                                }

                            }

                        }

                    case .Failure(let error):
                        reject(error)
                    }

            }

        }

    }

}


